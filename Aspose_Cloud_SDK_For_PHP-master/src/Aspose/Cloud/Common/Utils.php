<?php
/**
 * File to handle Utility Functions.
 *
 * @author Imran Anwar <imran.anwar@Aspose.com>
 * @author Assad Mahmood <assadvirgo@gmail.com>
 * @author Rvanlaak
 */

namespace Aspose\Cloud\Common;

use Aspose\Cloud\Exception\AsposeCloudException as Exception;

if (!function_exists('curl_init')) {
  throw new Exception('Aspose needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
  throw new Exception('Aspose needs the JSON PHP extension.');
}

/**
 * Provides access to the Aspose Platform.
 *
 * @author Imran Anwar <imran.anwar@Aspose.com>
 * @author Assad Mahmood <assadvirgo@gmail.com>
 * @author Rvanlaak
 */
class Utils {
  public static $httpCodes = array(
    100 => 'Continue',
    101 => 'Switching Protocols',
    102 => 'Processing',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    207 => 'Multi-Status',
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    306 => 'Switch Proxy',
    307 => 'Temporary Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Timeout',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Long',
    415 => 'Unsupported Media Type',
    416 => 'Requested Range Not Satisfiable',
    417 => 'Expectation Failed',
    418 => 'I\'m a teapot',
    422 => 'Unprocessable Entity',
    423 => 'Locked',
    424 => 'Failed Dependency',
    425 => 'Unordered Collection',
    426 => 'Upgrade Required',
    449 => 'Retry With',
    450 => 'Blocked by Windows Parental Controls',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Timeout',
    505 => 'HTTP Version Not Supported',
    506 => 'Variant Also Negotiates',
    507 => 'Insufficient Storage',
    509 => 'Bandwidth Limit Exceeded',
    510 => 'Not Extended',
  );

  /**
   * Performs Aspose Api Request.
   *
   * @param string $url
   *   Target Aspose API URL.
   * @param string $method
   *   Method to access the API such as GET, POST, PUT and DELETE.
   * @param string $header_type
   *   XML or JSON.
   * @param string $src
   *   Post data.
   */
  public static function processCommand($url, $method = 'GET', $header_type = 'XML', $src = '') {

    $method = strtoupper($method);
    $header_type = strtoupper($header_type);
    $session = curl_init();
    curl_setopt($session, CURLOPT_URL, $url);
    if ($method == 'GET') {
      curl_setopt($session, CURLOPT_HTTPGET, 1);
    }
    else {
      curl_setopt($session, CURLOPT_POST, 1);
      curl_setopt($session, CURLOPT_POSTFIELDS, $src);
      curl_setopt($session, CURLOPT_CUSTOMREQUEST, $method);
    }
    curl_setopt($session, CURLOPT_HEADER, FALSE);
    if ($header_type == 'XML') {
      curl_setopt($session, CURLOPT_HTTPHEADER, array('Accept: application/xml', 'Content-Type: application/xml'));
    }
    else {
      curl_setopt($session, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    }
    curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
    if (preg_match('/^(https)/i', $url)) {
      curl_setopt($session, CURLOPT_SSL_VERIFYPEER, FALSE);
    }
    $result = curl_exec($session);
    $header = curl_getinfo($session);

    if ($header['http_code'] != 200 && $header['http_code'] != 201) {
      // Throw throw new \Exception($result);
      return;
    }
    else {
      if (preg_match('/You have processed/i', $result) || preg_match('/Your pricing plan allows only/i', $result)) {
        throw new \Exception($result);
      }
    }
    curl_close($session);
    return $result;
  }

  /**
   * Performs Aspose Api Request to Upload a file.
   *
   * @param string $url
   *   Target Aspose API URL.
   * @param string $localfile
   *   Local file.
   * @param string $header_type
   *   XML or JSON.
   */
  public static function uploadFileBinary($url, $localfile, $header_type = 'XML') {

    $header_type = strtoupper($header_type);
    $fp = fopen($localfile, 'r');
    $session = curl_init();
    curl_setopt($session, CURLOPT_VERBOSE, 1);
    curl_setopt($session, CURLOPT_USERPWD, 'user:password');
    curl_setopt($session, CURLOPT_URL, $url);
    curl_setopt($session, CURLOPT_PUT, 1);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($session, CURLOPT_HEADER, FALSE);
    if ($header_type == 'XML') {
      curl_setopt($session, CURLOPT_HTTPHEADER, array('Accept: application/xml', 'Content-Type: application/xml'));
    }
    else {
      curl_setopt($session, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    }
    curl_setopt($session, CURLOPT_INFILE, $fp);
    curl_setopt($session, CURLOPT_INFILESIZE, filesize($localfile));
    $result = curl_exec($session);
    curl_close($session);
    fclose($fp);
    return $result;
  }

  /**
   * Encode a string to URL-safe base64.
   *
   * @param string $value
   *   Value to encode.
   */
  private static function encodeBase64UrlSafe($value) {
    return str_replace(array('+', '/'), array('-', '_'), base64_encode($value));
  }

  /**
   * Decode a string from URL-safe base64.
   *
   * @param string $value
   *   Value to Decode.
   */
  private static function decodeBase64UrlSafe($value) {
    return base64_decode(str_replace(array('-', '_'), array('+', '/'), $value));
  }

  /**
   * Sign Url.
   *
   * @param string $url_to_sign
   *   URL to Sign.
   */
  public static function sign($url_to_sign) {
    // Parse the url.
    $url_to_sign = rtrim($url_to_sign, "/");
    $url = parse_url($url_to_sign);

    $url_part_to_sign = $url['scheme'] . '://' . $url['host'] . str_replace(array(' ', '+'), array('%20', '%2B'), $url['path']);

    if (isset($url['query']) && !empty($url['query'])) {
      $url_part_to_sign .= "?" . str_replace(array(' ', '+'), array('%20', '%2B'), $url['query']) . '&appSID=' . AsposeApp::$appSID;
    }
    else {
      $url_part_to_sign .= '?appSID=' . AsposeApp::$appSID;
    }

    // Create a signature using the private key and the URL-encoded.
    // String using HMAC SHA1. This signature will be binary.
    $signature = hash_hmac('sha1', $url_part_to_sign, AsposeApp::$appKey, TRUE);

    $encoded_signature = self::encodeBase64UrlSafe($signature);
    $encoded_signature = str_replace(array(
      '=',
      '-',
      '_',
    ), array(
      '',
      '%2b',
      '%2f',
    ), $encoded_signature);

    preg_match_all("/%[0-9a-f]{2}/", $encoded_signature, $m);
    foreach ($m[0] as $code) {
      $encoded_signature = str_replace($code, strtoupper($code), $encoded_signature);
    }

    return $url_part_to_sign . '&signature=' . $encoded_signature;
  }

  /**
   * Will get the value of a field in JSON Response.
   *
   * @param string $json_response
   *   JSON Response string.
   * @param string $field_name
   *   Field to be found.
   *
   * @return getFieldValue($json_response, $field_name)
   *   String Value of the given Field.
   */
  public function getFieldValue($json_response, $field_name) {
    return json_decode($json_response)->{$field_name};
  }

  /**
   * This method parses XML for a count of a particular field.
   *
   * @param string $json_response
   *   JSON Response string.
   * @param string $field_name
   *   Field to be found.
   *
   * @return getFieldCount($json_response, $field_name)
   *   String Value of the given Field.
   */
  public function getFieldCount($json_response, $field_name) {
    $arr = json_decode($json_response)->{$field_name};
    return count($arr, COUNT_RECURSIVE);
  }

  /**
   * Copies the contents of input to output. Doesn't close either stream.
   *
   * @param string $input
   *   Input stream.
   *
   * @return copyStream($input)
   *   Outputs the converted input stream.
   */
  public function copyStream($input) {
    return stream_get_contents($input);
  }

  /**
   * Saves the files.
   *
   * @param string $input
   *   Input stream.
   * @param string $file_name
   *   FileName along with the full path.
   */
  public static function saveFile($input, $file_name) {
    $fh = fopen($file_name, 'w') or die('cant open file');
    fwrite($fh, $input);
    fclose($fh);
  }

  /**
   * Gets File name.
   *
   * @param string $file
   *   File to get name of.
   */
  public static function getFileName($file) {
    $info = pathinfo($file);
    $file_name = basename($file, '.' . $info['extension']);
    return $file_name;
  }

  /**
   * Validates Output.
   *
   * @param string $result
   *   Input to validate.
   */
  public static function validateOutput($result) {
    $string = (string) $result;
    $validate = array(
      'Unknown file format.',
      'Unable to read beyond the end of the stream',
      'Index was out of range',
      'Cannot read that as a ZipFile',
      'Not a Microsoft PowerPoint 2007 presentation',
      'Index was outside the bounds of the array',
      'An attempt was made to move the position before the beginning of the stream',
    );
    $invalid = 0;
    foreach ($validate as $value) {
      $pos = strpos($string, $value);
      if ($pos === 1) {
        $invalid = 1;
      }
    }
    if ($invalid == 1) {
      return $string;
    }
    else {
      return '';
    }
  }
}
