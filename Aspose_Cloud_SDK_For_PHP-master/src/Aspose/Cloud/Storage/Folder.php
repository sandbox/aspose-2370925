<?php
/**
 * Class providing methods to perform transactions on storage of Aspose App.
 */

namespace Aspose\Cloud\Storage;

use Aspose\Cloud\Common\Utils;
use Aspose\Cloud\Common\Product;
use Aspose\Cloud\Exception\AsposeCloudException as Exception;

class Folder {

  public $strURIFolder = '';
  public $strURIFile = '';
  public $strURIExist = '';
  public $strURIDisc = '';

  /**
   * Constructor function for Folder class.
   */
  public function __construct() {
    $this->strURIFolder = Product::$baseProductUri . '/storage/folder/';
    $this->strURIFile = Product::$baseProductUri . '/storage/file/';
    $this->strURIExist = Product::$baseProductUri . '/storage/exist/';
    $this->strURIDisc = Product::$baseProductUri . '/storage/disc';
  }

  /**
   * Uploads a local file to specified folder / subfolder on Aspose storage.
   *
   * @param string $str_file
   *   File Name.
   * @param string $str_folder
   *   Folder Name.
   */
  public function uploadFile($str_file, $str_folder, $storage_name = '') {
    $str_remote_file_name = basename($str_file);
    $str_uri_request = $this->strURIFile;
    if ($str_folder == '') {
      $str_uri_request .= $str_remote_file_name;
    }
    else {
      $str_uri_request .= $str_folder . '/' . $str_remote_file_name;
    }
    if ($storage_name != '') {
      $str_uri_request .= '?storage=' . $storage_name;
    }
    $signed_uri = Utils::sign($str_uri_request);

    Utils::uploadFileBinary($signed_uri, $str_file);
  }

  /**
   * Checks if a file exists.
   *
   * @param string $file_name
   *   File Name.
   */
  public function fileExists($file_name, $storage_name = '') {
    // Check whether file is set or not.
    if ($file_name == '') {
      throw new Exception('No file name specified');
    }

    // Build URI.
    $str_uri = $this->strURIExist . $file_name;
    if ($storage_name != '') {
      $str_uri .= '?storage=' . $storage_name;
    }
    // Sign URI.
    $signed_uri = Utils::sign($str_uri);

    $response_stream = json_decode(Utils::processCommand($signed_uri, 'GET', '', ''));
    if (!$response_stream->FileExist->IsExist) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes a file from remote storage.
   *
   * @param string $file_name
   *   File Name.
   */
  public function deleteFile($file_name, $storage_name = '') {
    // Check whether file is set or not.
    // Build URI.
    if ($file_name == '') {
      throw new Exception('No file name specified');
    }
    $str_uri = $this->strURIFile . $file_name;
    if ($storage_name != '') {
      $str_uri .= '?storage=' . $storage_name;
    }
    // Sign URI.
    $signed_uri = Utils::sign($str_uri);

    $response_stream = json_decode(Utils::processCommand($signed_uri, 'DELETE', '', ''));
    if ($response_stream->Code != 200) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Creates a new folder  under the specified folder on Aspose storage.
   *
   * If no path specified, creates a folder under the root folder.
   *
   * @param string $str_folder
   *   Folder Name.
   */
  public function createFolder($str_folder, $storage_name = '') {
    // Build URI.
    $str_uri_request = $this->strURIFolder . $str_folder;
    if ($storage_name != '') {
      $str_uri_request .= '?storage=' . $storage_name;
    }
    // Sign URI.
    $signed_uri = Utils::sign($str_uri_request);

    $response_stream = json_decode(Utils::processCommand($signed_uri, 'PUT', '', ''));

    if ($response_stream->Code != 200) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes a folder from remote storage.
   *
   * @param string $folder_name
   *   Folder Name.
   */
  public function deleteFolder($folder_name) {
    // Check whether folder is set or not.
    if ($folder_name == '') {
      throw new Exception('No folder name specified');
    }

    // Build URI.
    $str_uri = $this->strURIFolder . $folder_name;

    // Sign URI.
    $signed_uri = Utils::sign($str_uri);

    $response_stream = json_decode(Utils::processCommand($signed_uri, 'DELETE', '', ''));
    if ($response_stream->Code != 200) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Provides the total / free disc size in bytes for your app.
   */
  public function getDiscUsage($storage_name = '') {
    // Build URI.
    $str_uri = $this->strURIDisc;
    if ($storage_name != '') {
      $str_uri .= '?storage=' . $storage_name;
    }
    // Sign URI.
    $signed_uri = Utils::sign($str_uri);

    $response_stream = json_decode(Utils::processCommand($signed_uri, 'GET', '', ''));

    return $response_stream->DiscUsage;
  }

  /**
   * Get file from Aspose server.
   *
   * @param string $file_name
   *   File Name.
   */
  public function getFile($file_name, $storage_name = '') {
    // Check whether file is set or not.
    if ($file_name == '') {
      throw new Exception('No file name specified');
    }

    // Build URI.
    $str_uri = $this->strURIFile . $file_name;
    if ($storage_name != '') {
      $str_uri .= '?storage=' . $storage_name;
    }
    // Sign URI.
    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    return $response_stream;
  }

  /**
   * Retrives the list of files and folders under the specified folder.
   *
   * Use empty string to specify root folder.
   *
   * @param string $str_folder
   *   Folder Name.
   */
  public function getFilesList($str_folder, $storage_name = '') {

    // Build URI.
    $str_uri = $this->strURIFolder;
    // Check whether file is set or not.
    if (!$str_folder == '') {
      $str_uri .= $str_folder;
    }
    if ($storage_name != '') {
      $str_uri .= '?storage=' . $storage_name;
    }

    // Sign URI.
    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $json = json_decode($response_stream);

    return $json->Files;
  }
}
