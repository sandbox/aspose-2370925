<?php
/**
 * Converts pages or document into different formats.
 */

namespace Aspose\Cloud\Words;

use Aspose\Cloud\Common\AsposeApp;
use Aspose\Cloud\Common\Utils;
use Aspose\Cloud\Common\Product;
use Aspose\Cloud\Exception\AsposeCloudException as Exception;

class Converter {

  public $fileName = '';
  public $saveFormat = '';

  /**
   * Constructor for Converter class.
   */
  public function __construct($file_name) {
    // Set default values.
    $this->fileName = $file_name;

    $this->saveFormat = 'Doc';
  }

  /**
   * Convert a document to SaveFormat.
   */
  public function convert() {
    // Check whether file is set or not.
    if ($this->fileName == '') {
      throw new Exception('No file name specified');
    }
    // Build URI.
    $str_uri = Product::$baseProductUri . '/words/' . $this->fileName . '?format=' . $this->saveFormat;

    // Sign URI.
    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $v_output = Utils::validateOutput($response_stream);

    if ($v_output === '') {
      return $response_stream;
    }
    else {
      return $v_output;
    }
  }

  /**
   * Converts Local File.
   */
  public function convertLocalFile($input_path, $output_path, $output_format) {
    $str_uri = Product::$baseProductUri . '/words/convert?format=' . $output_format;
    $signed_uri = Utils::sign($str_uri);
    $response_stream = Utils::uploadFileBinary($signed_uri, $input_path, 'xml');

    $v_output = Utils::validateOutput($response_stream);

    if ($v_output === '') {
      if ($output_format == 'html') {
        $save_format = 'zip';
      }
      else {
        $save_format = $output_format;
      }

      if ($output_path == '') {
        $output_filename = Utils::getFileName($input_path) . '.' . $save_format;
      }

      Utils::saveFile($response_stream, AsposeApp::$outPutLocation . $output_filename);
      return $output_filename;
    }
    else {
      return $v_output;
    }
  }
}
