<?php
/**
 * Extract various types of information from the document.
 */

namespace Aspose\Cloud\Words;

use Aspose\Cloud\Common\AsposeApp;
use Aspose\Cloud\Common\Utils;
use Aspose\Cloud\Common\Product;
use Aspose\Cloud\Exception\AsposeCloudException as Exception;

class Extractor {
  public $fileName = '';

  /**
   * Constructor for Extractor.
   */
  public function __construct($file_name) {
    $this->fileName = $file_name;
  }

  /**
   * Gets Text items list from document.
   */
  public function getText() {
    // Check whether file is set or not.
    if ($this->fileName == '') {
      throw new Exception('No file name specified');
    }
    $str_uri = Product::$baseProductUri . '/words/' . $this->fileName . '/textItems';

    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $json = json_decode($response_stream);

    return $json->TextItems->List;
  }

  /**
   * Get the OLE drawing object from document.
   *
   * @param int $index
   *   Index Value.
   * @param string $ole_format
   *   OLE format.
   */
  public function getoleData($index, $ole_format) {
    // Check whether file is set or not.
    if ($this->fileName == '') {
      throw new Exception('No file name specified');
    }
    $str_uri = Product::$baseProductUri . '/words/' . $this->fileName . '/drawingObjects/' . $index . '/oleData';

    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $v_output = Utils::validateOutput($response_stream);

    if ($v_output === '') {
      $output_path = AsposeApp::$outPutLocation . Utils::getFileName($this->fileName) . '_' . $index . '.' . $ole_format;
      Utils::saveFile($response_stream, $output_path);
      return $output_path;
    }
    else {
      return $v_output;
    }
  }

  /**
   * Get the Image drawing object from document.
   *
   * @param int $index
   *   Index value.
   * @param string $render_format
   *   Render Format.
   */
  public function getimageData($index, $render_format) {
    // Check whether file is set or not.
    if ($this->fileName == '') {
      throw new Exception('No file name specified');
    }
    $str_uri = Product::$baseProductUri . '/words/' . $this->fileName . '/drawingObjects/' . $index . '/ImageData';

    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $v_output = Utils::validateOutput($response_stream);

    if ($v_output === '') {
      $output_path = AsposeApp::$outPutLocation . Utils::getFileName($this->fileName) . '_' . $index . '.' . $render_format;
      Utils::saveFile($response_stream, $output_path);
      return $output_path;
    }
    else {
      return $v_output;
    }
  }

  /**
   * Convert drawing object to image.
   *
   * @param int $index
   *   Index Value.
   * @param string $render_format
   *   Render Format.
   */
  public function convertDrawingObject($index, $render_format) {
    // Check whether file is set or not.
    if ($this->fileName == '') {
      throw new Exception('No file name specified');
    }
    $str_uri = Product::$baseProductUri . '/words/' . $this->fileName . '/drawingObjects/' . $index . '?format=' . $render_format;

    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $v_output = Utils::validateOutput($response_stream);

    if ($v_output === '') {
      $output_path = AsposeApp::$outPutLocation . Utils::getFileName($this->fileName) . '_' . $index . '.' . $render_format;
      Utils::saveFile($response_stream, $output_path);
      return $output_path;
    }
    else {
      return $v_output;
    }
  }

  /**
   * Get the List of drawing object from document.
   */
  public function getDrawingObjectList() {
    // Check whether file is set or not.
    if ($this->fileName == '') {
      throw new Exception('No file name specified');
    }
    $str_uri = Product::$baseProductUri . '/words/' . $this->fileName . '/drawingObjects';

    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $json = json_decode($response_stream);

    if ($json->Code == 200) {
      return $json->DrawingObjects->List;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the drawing object from document.
   *
   * @param string $object_uri
   *   Object URI.
   * @param string $output_path
   *   Output Path.
   */
  public function getDrawingObject($object_uri, $output_path) {
    // Check whether file is set or not.
    if ($this->fileName == '') {
      throw new Exception('No file name specified');
    }
    if ($output_path == '') {
      throw new Exception('Output path not specified');
    }
    if ($object_uri == '') {
      throw new Exception('Object URI not specified');
    }
    $url_arr = explode('/', $object_uri);
    $object_index = end($url_arr);

    $str_uri = $object_uri;

    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $json = json_decode($response_stream);

    if ($json->Code == 200) {
      if ($json->DrawingObject->ImageDataLink != '') {
        $str_uri = $str_uri . '/imageData';
        $output_path = $output_path . '\\DrawingObject_' . $object_index . '.jpeg';
      }
      elseif ($json->DrawingObject->OleDataLink != '') {
        $str_uri = $str_uri . '/oleData';
        $output_path = $output_path . '\\DrawingObject_' . $object_index . '.xlsx';
      }
      else {
        $str_uri = $str_uri . '?format=jpeg';
        $output_path = $output_path . '\\DrawingObject_' . $object_index . '.jpeg';
      }

      $signed_uri = Utils::sign($str_uri);

      $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

      $v_output = Utils::validateOutput($response_stream);

      if ($v_output === '') {
        Utils::saveFile($response_stream, $output_path);
        return $output_path;
      }
      else {
        return $v_output;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the List of drawing object from document.
   *
   * @param string $output_path
   *   Output Path.
   */
  public function getDrawingObjects($output_path) {
    // Check whether file is set or not.
    if ($this->fileName == '') {
      throw new Exception('No file name specified');
    }
    if ($output_path == '') {
      throw new Exception('Output path not specified');
    }
    $str_uri = Product::$baseProductUri . '/words/' . $this->fileName . '/drawingObjects';

    $signed_uri = Utils::sign($str_uri);

    $response_stream = Utils::processCommand($signed_uri, 'GET', '', '');

    $json = json_decode($response_stream);

    if ($json->Code == 200) {
      foreach ($json->DrawingObjects->List as $object) {
        $this->GetDrawingObject($object->link->Href, $output_path);
      }
    }
    else {
      return FALSE;
    }
  }
}
