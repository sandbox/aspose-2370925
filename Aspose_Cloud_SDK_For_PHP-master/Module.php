<?php
/**
 * Module class having an autoloader.
 */

namespace Aspose;

class Module {
  /**
   * Function to get Configurations.
   */
  public function getConfig() {
    return include __DIR__ . '/config/module.config.php';
  }

  /**
   * Function to get Autoloader Configurations.
   */
  public function getAutoloaderConfig() {
    return array(
      'Zend\Loader\StandardAutoloader' => array(
        'namespaces' => array(
          __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
        ),
      ),
    );
  }
}
